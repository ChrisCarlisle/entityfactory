<?php

namespace Jupix\EntityFactory;

interface IsPersistable
{

    /**
     * Build the entity with attributes.
     *
     * @param  string $type
     * @param  array  $attributes
     * @throws EntityFactoryException
     * @return mixed
     */
    public function build($type, array $attributes);

    /**
     * Persist the entity.
     *
     * @param  object $entity
     * @param  object $repository
     * @return mixed
     */
    public function save($entity, $repository);

    /**
     * Get all attributes for the model.
     *
     * @param  object $entity
     * @return array
     */
//    public function getAttributes($entity);

}
