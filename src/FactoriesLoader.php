<?php

namespace Jupix\EntityFactory;

class FactoriesLoader
{

    /**
     * Load the factories.
     *
     * @param  string $basePath
     * @return array
     */
    public function load($basePath)
    {
        $this->assertThatFactoriesDirectoryExists($basePath);

        $designer = new Designer;
        $faker = new FakerAdapter;

        $factory = function ($name, $repository, $shortName, $attributes = []) use ($designer, $faker) {
            return $designer->define($name, $repository, $shortName, $attributes);
        };

        foreach ((new FactoriesFinder($basePath))->find() as $file) {
            include($file);
        }

        return $designer->definitions();
    }

    /**
     * Assert that the given factories directory exists.
     *
     * @param  string $basePath
     * @return mixed
     * @throws EntityFactoryException
     */
    private function assertThatFactoriesDirectoryExists($basePath)
    {
        if ( ! is_dir($basePath)) {
            throw new EntityFactoryException(
                "The path provided for the factories directory, {$basePath}, does not exist."
            );
        }
    }
}
