<?php

namespace Jupix\EntityFactory;

class EntityModel implements IsPersistable
{
    /**
     * Build the entity with attributes.
     *
     * @param  string $type
     * @param  array  $attributes
     * @throws EntityFactoryException
     * @return mixed
     */
    public function build($type, array $attributes)
    {
        $model = new $type;

        // First, see if a createFrom() method exists
        if (method_exists($model, 'createFrom')) {
            return $model->createFrom($attributes);
        }

        // Otherwise, check for setters prefixed with 'set'
        foreach ($attributes as $parameter => $value) {

            // Prepend 'set' to the $parameter name, unless it already begins with set
            $setMethod = strpos($parameter, 'set') === 0 ?  $parameter : 'set' . ucfirst($parameter);

            if (method_exists($model, $setMethod)) {
                $model->$setMethod($value);
            } else {
                throw new EntityFactoryException("{$type} does not contain a {$setMethod} method");
            }
        }

        return $model;
    }

    /**
     * Persist the entity using a repository
     *
     * @param  object $entity
     * @param  object $repository
     * @throws EntityFactoryException
     * @return mixed
     */
    public function save($entity, $repository)
    {
        if (function_exists('app')) {
            // Use IoC Container, if available
            $repository = app()->make($repository);
        } else {
            // Otherwise assume we've directly been given a repository
            $repository = new $repository;
        }
        return $repository->save($entity);
    }

    /**
     * Get all attributes for the model.
     *
     * @param  object $entity
     * @return array
     */
    public function getAttributes($entity) {}

}
