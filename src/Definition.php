<?php

namespace Jupix\EntityFactory;

class Definition
{

    /**
     * The entity class name for the factory.
     *
     * @var string
     */
    public $name;

    /**
     * The repository class name for the factory.
     *
     * @var string
     */
    public $repository;

    /**
     * The abbreviated short-name.
     *
     * @var string
     */
    public $shortName;

    /**
     * Attributes for the factory.
     *
     * @var array
     */
    public $attributes;

    /**
     * Create a new Definition instance.
     *
     * @param string $name
     * @param string $repository
     * @param string $shortName
     * @param array  $attributes
     */
    public function __construct($name, $repository, $shortName, $attributes = [])
    {
        $this->name = $name;
        $this->repository = $repository;
        $this->shortName = $shortName;
        $this->attributes = $attributes;
    }

}
