<?php

class PostRepository
{
    public function save(Post $post)
    {

        $eloquentPost = EloquentPost::findOrNew($post->getID());

        $eloquentPost->title = $post->getTitle();
        $eloquentPost->author_id = $post->getAuthor_id();

        $eloquentPost->save();

        return $post->set_id($eloquentPost->id);
    }
}