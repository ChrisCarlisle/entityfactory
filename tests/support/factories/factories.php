<?php

$factory('Post', 'PostRepository', 'scheduled_post', [
    'title' => 'Scheduled Post Title'
]);

$factory('Post', 'PostRepository', [
    'author_id' => 'factory:Person',
    'title' => 'Post Title'
]);

$factory('Comment', 'CommentRepository', function($faker) {

    return [
//        'post_id' => 'factory:Post',
        'body' => $faker->word
    ];
});

$factory('Comment', 'CommentRepository', 'comment', []);

$factory('Comment', 'CommentRepository', 'comment_for_post_by_person', [
    'post_id' => 'factory:Post',
    'body' => $faker->word
]);

$factory('Foo', 'FooRepository', function($faker) {
    return [
        'name' => $faker->word
    ];
});

$factory('Message', 'MessageRepository', [
    'contents' => $faker->sentence,
    'sender_id' => 'factory:Person',
    'receiver_id' => 'factory:Person',
]);

$factory('Person', 'CommentRepository', [
    'name' => $faker->name
]);
