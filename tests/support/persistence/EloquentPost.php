<?php

use \Illuminate\Database\Eloquent\Model;

class EloquentPost extends Model {
    protected $table = 'posts';
    public function comments() { return $this->hasMany('Comment'); }
    public function author() { return $this->belongsTo('Person', 'author_id'); }
}
