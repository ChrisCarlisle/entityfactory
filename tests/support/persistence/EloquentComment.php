<?php

use \Illuminate\Database\Eloquent\Model;

class EloquentComment extends Model {
    public function post() { return $this->belongsTo('Post'); }
}