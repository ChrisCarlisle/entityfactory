<?php

class Post
{
    protected $id;
    protected $author_id;
    protected $title;

    public function set_id($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setAuthor_id($author_id)
    {
        $this->author_id = $author_id;

        return $this;
    }

    public function getAuthor_id()
    {
        return $this->author_id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }
}