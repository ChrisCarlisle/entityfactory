<?php

class Comment
{
    protected $post_id;
    protected $body;

    public function setPost_id($post_id)
    {
        $this->post_id = $post_id;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getBody()
    {
        return $this->body;
    }
}